﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject projectile;
    public float bulletSpeed = 100f;

    bool Shot = false;
    void Fire()
    {
        GameObject instProjectile = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        Rigidbody body = instProjectile.GetComponent<Rigidbody>();
        body.AddForce(transform.forward * bulletSpeed);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !Shot)
        {
            Shot = true;
            Fire();
            print("Shooting");
            Shot = false;
        }
    }

}
