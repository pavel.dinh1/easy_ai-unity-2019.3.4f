﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 12f;

    public CharacterController controller;

    float x;
    float z;

    private void Update()
    {
        // Movement
        z = Input.GetAxis("Horizontal");
        x = Input.GetAxis("Vertical");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // process movement
        Vector3 move = transform.right * -x + transform.forward * z;
        controller.Move(move * speed * Time.fixedDeltaTime);
    }
}
