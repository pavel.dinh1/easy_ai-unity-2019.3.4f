﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public enum EnemyState
    {
        None,
        Patrolling,
        Chasing
    }
    public EnemyState state = EnemyState.None;
    public float lookRadius = 10f;

    [SerializeField]
    List<Waypoint> patrols = null;

    public int patrolIndex = 0;
    public float remainingDistance = 2f;
    Transform target;
    NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        // Chase the player if register them     
        if (distance <= lookRadius)
        {
            state = EnemyState.Chasing;
        }
        // Go back to patrolling        
        else if (patrols != null && patrols.Count >= 0)
        {
            state = EnemyState.Patrolling;
        }

        switch (state)
        {
            case EnemyState.Patrolling:
                if (!agent.pathPending && agent.remainingDistance <= remainingDistance)
                {
                    SetDestination();
                }
                break;
            case EnemyState.Chasing:
                agent.SetDestination(target.position);
                break;
            default:
                print("Error should never execute");
                break;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    void SetDestination()
    {
        agent.destination = patrols[patrolIndex].transform.position;
        patrolIndex = (patrolIndex + 1) % patrols.Count;
    }
}
