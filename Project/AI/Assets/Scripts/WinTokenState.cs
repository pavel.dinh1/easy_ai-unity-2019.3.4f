﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTokenState : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}
