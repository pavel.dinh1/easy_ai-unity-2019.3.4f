﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public bool GameEnded = false;
    float TotalTimer = 3f;
    float CurrentTimer = 0f;
    public GameObject[] Enemies;

    #region Singleton
    public static PlayerManager instance;
    private void Awake()
    {
        instance = this;
    }
    #endregion

    private void Start()
    {
        Enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    public GameObject player;
    public GameObject Canvas;
    public GameObject WinToken;
    void Update()
    {
        if (WinToken == null)
        {
            Canvas.GetComponentInChildren<Text>().text = "YOU WIN!";
            GameEnded = true;
            AllKill();
        }
        if (player == null && WinToken != null)
        {
            Canvas.GetComponentInChildren<Text>().text = "YOU LOST!";
            GameEnded = true;
            AllKill();
        }
        if (GameEnded)
        {
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer >= TotalTimer)
            {
                Application.Quit();
                print("Ended");
            }
        }
    }

    public void AllKill()
    {
        foreach (GameObject Enemy in Enemies)
        {
            Destroy(Enemy);
        }
        Destroy(player);
    }
}
